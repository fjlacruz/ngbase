# Usar la imagen oficial de Node.js versión 18
FROM node:18

# Establecer el directorio de trabajo
WORKDIR /app

# Instalar Angular CLI globalmente
RUN npm install -g @angular/cli@16

# Copiar el archivo package.json y package-lock.json
COPY package*.json ./

# Instalar las dependencias del proyecto
RUN npm ci --only=production --legacy-peer-deps

# Copiar el resto de los archivos de la aplicación
COPY . .

# Exponer el puerto 5000
EXPOSE 5000

# Comando para ejecutar la aplicación
CMD ["npm", "run", "dev"]


