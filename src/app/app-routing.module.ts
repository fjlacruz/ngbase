import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/guards/auth.guard';

const routes: Routes = [
{ path: 'home', loadChildren: () => import('./components/home/home.module').then(m => m.HomeModule) },
//{ path: 'contacto', loadChildren: () => import('./components/contacto/contacto.module').then(m => m.ContactoModule),canActivate: [AuthGuard] },
{ path: 'contacto', loadChildren: () => import('./components/contacto/contacto.module').then(m => m.ContactoModule)},
{ path: '', loadChildren: () => import('./components/home/home.module').then(m => m.HomeModule) }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
