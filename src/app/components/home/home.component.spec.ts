import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HomeComponent } from './home.component';
import { ShareModule } from 'src/app/share/share/share.module';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { SidebarModule } from 'primeng/sidebar';
import { RouterTestingModule } from '@angular/router/testing';

describe('HomeComponent', () => {
 let component: HomeComponent;
 let fixture: ComponentFixture<HomeComponent>;

 // Configuración del módulo de prueba
 beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeComponent ],
      imports: [ ShareModule, ButtonModule, DialogModule, SidebarModule, RouterTestingModule ],
    }).compileComponents();
 });

 // Creación del componente
 beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
 });

 // Pruebas de creación del componente
 describe('Component Creation', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });
 });

 // Pruebas de interacción del componente
 describe('Component Interaction', () => {
    it('should set visible to true when showDialog is called', () => {
      expect(component.visible).toBeFalse();
      component.showDialog();
      expect(component.visible).toBeTrue();
    });
 });
});
