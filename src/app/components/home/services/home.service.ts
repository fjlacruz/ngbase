import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { User } from '../../../core/models/user'; // Asegúrate de que esta ruta sea correcta
import { UserResponse } from '../../../core/interfaces/usuario';
import { environment } from '../../../../environments/environment';

@Injectable({
 providedIn: 'root'
})
export class HomeService {
 private apiUrl = environment.apiUrl;

 constructor(private http: HttpClient) { }

 getUsers(): Observable<User[]> {
  console.log(environment.env)
    return this.http.get<UserResponse>(this.apiUrl).pipe(
      map(response => response.data.map(userData => new User(userData))),
      catchError(this.handleError)
    );
 }

 private handleError(error: any) {
    console.error('Error en HomeService:', error);
    return throwError('Error al obtener los usuarios');
 }
}
