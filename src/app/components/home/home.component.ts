import { Component, OnInit } from '@angular/core';
import { HomeService } from '../home/services/home.service';
import { User } from '../../core/models/user';

@Component({
 selector: 'app-home',
 templateUrl: './home.component.html',
 styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
 users: User[] = [];
 visible: boolean = false;
 sidebarVisible: boolean = false;
 errorMessage: string | null = null;

 constructor(private homeService: HomeService) { }

 ngOnInit(): void {
    this.loadUsers();
 }

 private loadUsers(): void {
    this.homeService.getUsers().subscribe(
      this.handleSuccess,
      this.handleError
    );
 }

 private  handleSuccess = (users: User[]): void => {
    this.users = users;
    console.log(this.users);
    this.errorMessage = null;
 };

 private handleError = (error: any): void => {
    console.error('Error al cargar usuarios:', error);
    this.errorMessage = 'Ocurrió un error al cargar los usuarios. Por favor, inténtalo de nuevo más tarde.';
 };

 public showDialog(): void {
    this.visible = true;
 }
}
