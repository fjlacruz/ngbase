
// user-response.interface.ts
export interface UserResponse {
  data: User[];
 }

 // user.interface.ts
 export interface User {
  id: number;
  email: string;
  first_name: string;
  last_name: string;
  avatar: string;
 }
