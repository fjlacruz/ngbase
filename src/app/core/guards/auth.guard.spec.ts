import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { of } from 'rxjs';
import { AuthGuard } from '../guards/auth.guard'; // Asegúrate de importar tu guardia

describe('AuthGuard', () => {
 let guard: AuthGuard;

 // Configuración del módulo de prueba
 beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [AuthGuard]
    });
    guard = TestBed.inject(AuthGuard);
 });

 // Pruebas de autorización
 describe('Authorization', () => {
    it('should allow navigation if canActivate returns true', () => {
      // Crear instancias simuladas de ActivatedRouteSnapshot y RouterStateSnapshot
      const route: ActivatedRouteSnapshot = new ActivatedRouteSnapshot();
      const state: RouterStateSnapshot = jasmine.createSpyObj('RouterStateSnapshot', ['toString']);

      // Llamar al método canActivate y verificar el resultado
      const result = guard.canActivate(route, state);
      expect(result).toBeTrue();
    });

    // Aquí puedes agregar más pruebas relacionadas con la autorización
 });
});
