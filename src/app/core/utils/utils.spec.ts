import { Utils } from './utils';

describe('Utils', () => {
 it('should format date', () => {
    const date = new Date();
    const formattedDate = Utils.formatDate(date);
    expect(formattedDate).toBe(date.toISOString());
 });

 // Añade más pruebas aquí
});
