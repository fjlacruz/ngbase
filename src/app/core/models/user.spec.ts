import { User } from './user';

describe('User', () => {
 it('should create a user', () => {
    const user = new User(1, 'John Doe', 'john.doe@example.com');
    expect(user.id).toEqual(1);
    expect(user.name).toEqual('John Doe');
    expect(user.email).toEqual('john.doe@example.com');
 });

 // Añade más pruebas aquí
});
