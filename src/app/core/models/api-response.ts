import { User } from "./user";

export class ApiResponse {
  page: number;
  per_page: number;
  total: number;
  total_pages: number;
  data: User[];

  constructor(data: any) {
     this.page = data.page;
     this.per_page = data.per_page;
     this.total = data.total;
     this.total_pages = data.total_pages;
     this.data = data.data.map((userData: User[]) => new User(userData));
  }
 }
