// user.model.ts
export class User {
  id: number;
  email: string;
  first_name: string;
  last_name: string;
  avatar: string;

  constructor(data: any) {
     this.id = data.id;
    this.email = data.email;
    this.first_name = data.first_name;
    this.last_name = data.last_name;
    this.avatar = data.avatar;
  }

  // Método para validar el correo electrónico
  isValidEmail(): boolean {
     const emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
     return emailRegex.test(this.email);
  }

  // Método para obtener el nombre completo
  getFullName(): string {
     return `${this.first_name} ${this.last_name}`;
  }
 }
