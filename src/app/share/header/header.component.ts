import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/api';

@Component({
 selector: 'app-header',
 templateUrl: './header.component.html',
 styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
 items: MenuItem[] | undefined;
 constructor(
    private router: Router,
 ) {}
 ngOnInit(): void {
    this.items = [
            {
                label: 'File',
                icon: 'pi pi-fw pi-file',
                items: [
                    {
                        label: 'New',
                        icon: 'pi pi-fw pi-plus',
                        items: [
                            {
                                label: 'Bookmark',
                                icon: 'pi pi-fw pi-bookmark',
                                routerLink: '/bookmark' // Asegúrate de que esta ruta esté definida en tu módulo de rutas
                            },
                            {
                                label: 'Video',
                                icon: 'pi pi-fw pi-video',
                                routerLink: '/video' // Asegúrate de que esta ruta esté definida en tu módulo de rutas
                            }
                        ]
                    },
                    {
                        label: 'Delete',
                        icon: 'pi pi-fw pi-trash',
                        routerLink: '/delete' // Asegúrate de que esta ruta esté definida en tu módulo de rutas
                    },
                    {
                        separator: true
                    },
                    {
                        label: 'Export',
                        icon: 'pi pi-fw pi-external-link',
                        routerLink: '/export' // Asegúrate de que esta ruta esté definida en tu módulo de rutas
                    }
                ]
            },
            {
                label: 'Edit',
                icon: 'pi pi-fw pi-pencil',
                items: [
                    {
                        label: 'Left',
                        icon: 'pi pi-fw pi-align-left',
                        routerLink: '/align-left' // Asegúrate de que esta ruta esté definida en tu módulo de rutas
                    },
                    {
                        label: 'Right',
                        icon: 'pi pi-fw pi-align-right',
                        routerLink: '/align-right' // Asegúrate de que esta ruta esté definida en tu módulo de rutas
                    },
                    {
                        label: 'Center',
                        icon: 'pi pi-fw pi-align-center',
                        routerLink: '/align-center' // Asegúrate de que esta ruta esté definida en tu módulo de rutas
                    },
                    {
                        label: 'Justify',
                        icon: 'pi pi-fw pi-align-justify',
                        routerLink: '/align-justify' // Asegúrate de que esta ruta esté definida en tu módulo de rutas
                    }
                ]
            },
            {
                label: 'Users',
                icon: 'pi pi-fw pi-user',
                items: [
                    {
                        label: 'New',
                        icon: 'pi pi-fw pi-user-plus',
                        routerLink: '/new-user' // Asegúrate de que esta ruta esté definida en tu módulo de rutas
                    },
                    {
                        label: 'Delete',
                        icon: 'pi pi-fw pi-user-minus',
                        routerLink: '/delete-user' // Asegúrate de que esta ruta esté definida en tu módulo de rutas
                    },
                    {
                        label: 'Search',
                        icon: 'pi pi-fw pi-users',
                        items: [
                            {
                                label: 'Filter',
                                icon: 'pi pi-fw pi-filter',
                                items: [
                                    {
                                        label: 'Print',
                                        icon: 'pi pi-fw pi-print',
                                        routerLink: '/print' // Asegúrate de que esta ruta esté definida en tu módulo de rutas
                                    }
                                ]
                            },
                            {
                                icon: 'pi pi-fw pi-bars',
                                label: 'List',
                                routerLink: '/user-list' // Asegúrate de que esta ruta esté definida en tu módulo de rutas
                            }
                        ]
                    }
                ]
            },
            {
                label: 'Events',
                icon: 'pi pi-fw pi-calendar',
                items: [
                    {
                        label: 'Edit',
                        icon: 'pi pi-fw pi-pencil',
                        items: [
                            {
                                label: 'Save',
                                icon: 'pi pi-fw pi-calendar-plus',
                                routerLink: '/save-event' // Asegúrate de que esta ruta esté definida en tu módulo de rutas
                            },
                            {
                                label: 'Delete',
                                icon: 'pi pi-fw pi-calendar-minus',
                                routerLink: '/delete-event' // Asegúrate de que esta ruta esté definida en tu módulo de rutas
                            }
                        ]
                    },
                    {
                        label: 'Archieve',
                        icon: 'pi pi-fw pi-calendar-times',
                        items: [
                            {
                                label: 'Remove',
                                icon: 'pi pi-fw pi-calendar-minus',
                                routerLink: '/remove-event' // Asegúrate de que esta ruta esté definida en tu módulo de rutas
                            }
                        ]
                    }
                ]
            },
            {
                label: 'Contacto',
                icon: 'pi pi-fw pi-power-off',
                routerLink: 'contacto' // Asegúrate de que esta ruta esté definida en tu módulo de rutas
            },
            {
              label: 'Home',
              icon: 'pi pi-fw pi-power-off',
              routerLink: '' // Asegúrate de que esta ruta esté definida en tu módulo de rutas
          },

        ];
    }
}
