import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MenubarModule } from 'primeng/menubar';
import { HeaderComponent } from './header.component';
import { ActivatedRoute } from '@angular/router';

describe('HeaderComponent', () => {
 let component: HeaderComponent;
 let fixture: ComponentFixture<HeaderComponent>;

 beforeEach(async () => {
     await TestBed.configureTestingModule({
       declarations: [ HeaderComponent ],
       imports: [ MenubarModule ],
       providers: [
         { provide: ActivatedRoute, useValue: { /* tus valores mock aquí */ } }
       ]
     })
     .compileComponents();
 });

 beforeEach(() => {
     fixture = TestBed.createComponent(HeaderComponent);
     component = fixture.componentInstance;
     fixture.detectChanges();
 });

 it('should create', () => {
     expect(component).toBeTruthy();
 });
});
