#!/bin/bash

# Función para construir la imagen Docker
manage_dependencies() {
 curerent_dir="$PWD"
 project_name=$(basename "$(basename "$curerent_dir")")
 echo "manage_dependencies"
 cd ..
 git clone https://gitlab.com/fjlacruz/dependencies.git
 cd dependencies
 mv node_modules ../$project_name/
 echo $PWD
 cd ..
 #rm -r dependencies
}


build_image() {
 curerent_dir="$PWD"
 image_name=$(basename "$(basename "$curerent_dir")")
 docker build -t $image_name:latest .
 echo "Image created: $image_name:latest"
}

# Función para ejecutar la imagen Docker con el directorio del proyecto y el puerto
execute_with_path() {
    local project_path=$1
    local project_name=$(basename "$(basename "$1")")
    local port=$2

  echo "run --rm -v "$project_path":/app -p $port:5000 -d $project_name:latest"
  docker run --rm -v "$project_path":/app -p $port:5000 -d $project_name:latest
}

# Función para solicitar al usuario que ingrese el directorio del proyecto y el puerto
run_image() {
    read -p "Ingrese la ruta completa del proyecto: " project_path
    while true; do
        read -p "Ingrese el puerto para ejecutar el contenedor (1024-49151): " port
        if [[ $port =~ ^[0-9]+$ ]] && [ $port -ge 1024 ] && [ $port -le 49151 ]; then
            break
        else
            echo "Puerto inválido. Por favor, ingrese un puerto en el rango 1024-49151."
        fi
    done
    if [ -d "$project_path" ]; then
        execute_with_path "$project_path" "$port"
    else
        echo "El directorio no existe. Por favor, ingrese una ruta válida."
    fi
}

# Mostrar el menú de opciones
show_menu() {
    echo -e "\e[32mSeleccione una opción:\e[0m"
    echo "0. gestionar dependencias"
    echo "1. Construir imagen"
    echo "2. Ejecutar imagen"
    echo "3. Salir"
}

# Loop principal
while true; do
    show_menu
    read -p "Ingrese el número de opción: " option
    case $option in
        0)
            manage_dependencies
            ;;
        1)
            build_image
            ;;
        2)
            run_image
            ;;
        3)
            echo "Saliendo..."
            exit 0
            ;;
        *)
            echo "Opción no válida. Por favor, ingrese un número válido."
            ;;
    esac
done
