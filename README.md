# Ngbase V1
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.2.13.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.


## Docker build an run

docker build -t ngbase:latest .  

docker run --rm -v "C:/ruta/absoluta/a/tu/proyecto/nombre-proycto":/app -p 5000:5000 -d imagen:tag    
docker run --rm -v "C:/Users/idsis/OneDrive/Escritorio/laboratorios/ANGULAR/ngbase":/app -p 5000:5000 -d ngbase:latest  

docker run --rm -v $(pwd):/app -p 5000:5000 -d ngbase:latest

docker run -p 5000:5000 -v $(pwd)/node_modules:/usr/src/app/node_modules -v $(pwd):/usr/src/app -d your-image-name  
docker run -p 6000:5000 -v "C:/Users/idsis/OneDrive/Escritorio/laboratorios/ANGULAR/test/ngbase/node_modules":/usr/src/app/node_modules -v "C:/Users/idsis/OneDrive/Escritorio/laboratorios/ANGULAR/test/ngbase":/usr/src/app -d ngbase:latest  


docker tag ngbase idsistemas15/ngbase:latest
docker push idsistemas15/ngbase:latest


